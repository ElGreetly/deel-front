import React from 'react'
import { AppBar, Typography, Toolbar, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  menuDiv: {
    width: '70%',
    margin: 'auto'
  },
  menuButton: {
    flex: 1,
    margin: '0 5% 0 5%'
  }
}))

function Navbar () {
  const classes = useStyles()
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h5" className={classes.title}>
                Deel
        </Typography>
        <div className={classes.menuDiv}>
          <Button variant="text" color="inherit" className={classes.menuButton}>
                Contracts
          </Button>
          <Button variant="text" color="inherit" className={classes.menuButton}>
                Jobs
          </Button>
          <Button variant="text" color="inherit" className={classes.menuButton}>
                Profiles
          </Button>
          <Button variant="text" color="inherit" className={classes.menuButton}>
                Admin
          </Button>
        </div>
      </Toolbar>
    </AppBar>
  )
}

export default Navbar
